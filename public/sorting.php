<?php

require __DIR__ . '/../vendor/autoload.php';

use App\Entity\Waste\WasteDay;
use App\Entity\WasteTreatment\GlassTreatment;
use App\Entity\WasteTreatment\Incinerator;
use App\Entity\WasteTreatment\MetalTreatment;
use App\Entity\WasteTreatment\PaperTreatment;
use App\Service\SortingService;

$wasteDay = new WasteDay(file_get_contents('data.json'));
$sortingService = new SortingService();

$type = ['papier', 'plastiques', 'organique', 'metaux', 'verre', 'autre'];
$plasticSort = ['PET', 'PVC', 'PC', 'PEHD'];

$plasticWasteArrayPET = [];
$plasticWasteArrayPVC = [];
$plasticWasteArrayPC = [];
$plasticWasteArrayPEHD = [];

$paperWasteArray = [];
$organicWasteArray = [];
$metalWasteArray = [];
$glassWasteArray = [];
$greyWasteArray = [];

$incinerator = [];

foreach ($wasteDay->wasteForADay() as $waste) {

    $paperWasteArray = $sortingService->sortWaste($paperWasteArray, $waste, $type[0]);
    $organicWasteArray = $sortingService->sortWaste($organicWasteArray, $waste, $type[2]);
    $metalWasteArray = $sortingService->sortWaste($metalWasteArray, $waste, $type[3]);
    $glassWasteArray = $sortingService->sortWaste($glassWasteArray, $waste, $type[4]);
    $greyWasteArray = $sortingService->sortWaste($greyWasteArray, $waste, $type[5]);

    $plasticWasteArrayPET = $sortingService->sortWaste($plasticWasteArrayPET, $waste, $type[1], $plasticSort[0]);
    $plasticWasteArrayPVC = $sortingService->sortWaste($plasticWasteArrayPVC, $waste, $type[1], $plasticSort[1]);
    $plasticWasteArrayPC = $sortingService->sortWaste($plasticWasteArrayPC, $waste, $type[1], $plasticSort[2]);
    $plasticWasteArrayPEHD = $sortingService->sortWaste($plasticWasteArrayPEHD, $waste, $type[1], $plasticSort[3]);
}

foreach ($wasteDay->wasteTreatmentForADay() as $wasteTreatment) {
    if ($wasteTreatment["type"] == "incinerateur") {
        array_push($incinerator, new Incinerator($wasteTreatment["ligneFour"], $wasteTreatment["capaciteLigne"]));
    }

    if ($wasteTreatment["type"] == "recyclagePapier") {
        $paperTreatment = new PaperTreatment($wasteTreatment["capacite"]);
    }

    if ($wasteTreatment["type"] == "recyclageVerre") {
        $glassTreatment = new GlassTreatment($wasteTreatment["capacite"]);
    }

    if ($wasteTreatment["type"] == "recyclageMetaux") {
        $metalTreatment = new MetalTreatment($wasteTreatment["capacite"]);
    }
}

/** @var App\Entity\Waste\CardBoardWaste $paperWaste */
foreach ($paperWasteArray as $paperWaste) {
    $paperTreatment->treatment($paperWaste);
}

/** @var App\Entity\Waste\GlassWaste $glassWaste */
foreach ($glassWasteArray as $glassWaste) {
    $glassTreatment->treatment($glassWaste);
}

/** @var App\Entity\Waste\MetalWaste $metalWaste */
foreach ($metalWasteArray as $metalWaste) {
    $metalTreatment->treatment($metalWaste);
}

