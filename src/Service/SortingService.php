<?php

namespace App\Service;

use App\Entity\Waste\CardboardWaste;
use App\Entity\Waste\GlassWaste;
use App\Entity\Waste\GreyWaste;
use App\Entity\Waste\MetalWaste;
use App\Entity\Waste\OrganicWaste;
use App\Entity\Waste\PlasticWaste;


class SortingService {

    public function sortWaste(array $array, array $waste, string $type, string $plasticType = null) :  array {

        switch ($type) {
            case 'papier':
                array_push($array, new CardboardWaste($type, $waste[$type]));
                return $array;
            break;

            case 'plastiques' :
                array_push($array, new PlasticWaste($type, $waste[$type][$plasticType], $plasticType));
                return $array;            
            break;

            case 'organique':
                array_push($array, new OrganicWaste($type, $waste[$type]));
                return $array;
            break;

            case 'metaux':
                array_push($array, new MetalWaste($type, $waste[$type]));
                return $array;
            break;

            case 'verre':
                array_push($array, new GlassWaste($type, $waste[$type]));
                return $array;
            break;

            case 'autre':
                array_push($array, new GreyWaste($type, $waste[$type]));
                return $array;
            break;
            
        }
    }
}