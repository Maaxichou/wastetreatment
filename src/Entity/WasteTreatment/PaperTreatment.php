<?php

namespace App\Entity\WasteTreatment;

use App\Entity\Waste\CardboardWaste;

class PaperTreatment {

    private $capacity;
    private $container = [];
    private $wasteLeft = [];
    private $weight = 0;

    public function __construct(int $capacity) {
       $this->capacity = $capacity;
       $this->container;
       $this->wasteLeft;
       $this->weight;
    }

    public function treatment(CardboardWaste $paperWaste)
    {
        if ($this->weight + $paperWaste->getWeight() <= $this->capacity) {
            array_push($this->container, $paperWaste);
            $this->weight += $paperWaste->getWeight();
        } else {
            array_push($this->wasteLeft, $paperWaste);
        }
    }

    public function getContainer() : array
    {
        return $this->container;
    }

    public function getWeight() : float
    {
        return $this->weight;
    }

    public function getCapacity() : float
    {
        return $this->capacity;
    }

    public function getWasteLeft() : array
    {
        return $this->wasteLeft;
    }
}