<?php

namespace App\Entity\WasteTreatment;

class Incinerator {

    private $ovenLines;
    private $capacity;

    public function __construct(int $ovenLines, int $capacity) {
       $this->ovenLines = $ovenLines;
       $this->capacity = $capacity;
       $this->size = $capacity*$ovenLines;
    }

}