<?php

namespace App\Entity\WasteTreatment;

use App\Entity\Waste\MetalWaste;

class MetalTreatment {

    private $capacity;
    private $container = [];
    private $wasteLeft = [];
    private $weight = 0;

    public function __construct(int $capacity) {
       $this->capacity = $capacity;
       $this->container;
       $this->wasteLeft;
       $this->weight;
    }

    public function treatment(MetalWaste $metalWaste)
    {
        if ($this->weight + $metalWaste->getWeight() <= $this->capacity) {
            array_push($this->container, $metalWaste);
            $this->weight += $metalWaste->getWeight();
        } else {
            array_push($this->wasteLeft, $metalWaste);
        }
    }

    public function getContainer() : array
    {
        return $this->container;
    }

    public function getWeight() : float
    {
        return $this->weight;
    }

    public function getCapacity() : float
    {
        return $this->capacity;
    }

    public function getWasteLeft() : array
    {
        return $this->wasteLeft;
    }
}