<?php

namespace App\Entity\WasteTreatment;

use App\Entity\Waste\GlassWaste;

class GlassTreatment {

    private $capacity;
    private $container = [];
    private $wasteLeft = [];
    private $weight = 0;

    public function __construct(int $capacity) {
       $this->capacity = $capacity;
       $this->container;
       $this->wasteLeft;
       $this->weight;
    }

    public function treatment(GlassWaste $glassWaste)
    {
        if ($this->weight + $glassWaste->getWeight() <= $this->capacity) {
            array_push($this->container, $glassWaste);
            $this->weight += $glassWaste->getWeight();
        } else {
            array_push($this->wasteLeft, $glassWaste);
        }
    }

    public function getContainer() : array
    {
        return $this->container;
    }

    public function getWeight() : float
    {
        return $this->weight;
    }

    public function getCapacity() : float
    {
        return $this->capacity;
    }

    public function getWasteLeft() : array
    {
        return $this->wasteLeft;
    }
}