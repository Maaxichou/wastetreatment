<?php

namespace App\Entity\Waste;

class PlasticWaste {

    private $type;
    private $weight;
    private $sort;

    public function __construct(string $type, float $weight, string $sort) {
        $this->weight = $weight;
        $this->type = $type;
        $this->sort = $sort;
    }

    
}