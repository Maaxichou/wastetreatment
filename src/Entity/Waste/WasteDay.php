<?php

namespace App\Entity\Waste;

class WasteDay
{

    public function __construct($file)
    {
        $this->file = $file;
    }

    public function wasteForADay()
    {
        $wasteArray = json_decode($this->file, true);
        return $wasteArray["quartiers"];
    }

    public function wasteTreatmentForADay()
    {
        $wasteTreatmentArray = json_decode($this->file, true);
        return $wasteTreatmentArray["services"];
    }
}
