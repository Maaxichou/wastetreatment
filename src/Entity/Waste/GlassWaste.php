<?php

namespace App\Entity\Waste;

class GlassWaste {

    private $type;
    private $weight;
    
    public function __construct(string $type, float $weight) {
        $this->type = $type;
        $this->weight = $weight;
    }

    public function getWeight() : float
    {
        return $this->weight;
    }
}