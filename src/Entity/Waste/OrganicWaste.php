<?php

namespace App\Entity\Waste;

class OrganicWaste {

    private $type;
    private $weight;
    
    public function __construct(string $type, float $weight) {
        $this->type = $type;
        $this->weight = $weight;
 
    }
}