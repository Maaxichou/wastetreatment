<?php

namespace App\Entity\Waste;

class GreyWaste {

    private $type;
    private $weight;
    
    public function __construct(string $type, float $weight) {
        $this->type = $type;
        $this->weight = $weight;
    }
}